import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Button, SafeAreaView,Alert} from 'react-native';
import Icon, {} from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';

export class LoginScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email : null,
            password : null,
            clearEmail : null,
            clearPassword : null,
            hidePassword : true
        }
    }
    
    async loginUser1(){
        const isLogin = "login"
        const isLogout = "logout"
        await AsyncStorage.setItem('user1', isLogin)
        await AsyncStorage.setItem('user2', isLogout)
    }

    async loginUser2(){
        const isLogin = "login"
        const isLogout = "logout"
        await AsyncStorage.setItem('user1', isLogout)
        await AsyncStorage.setItem('user2', isLogin)
    }

    async loginFunction(email,password){
        const email1 = "user1@gmail.com"
        const password1 = "password123"
        const email2 = "user2@gmail.com"
        const password2 = "password321"

        var emailInput = this.state.email
        var passwordInput = this.state.password

        if(emailInput==null||passwordInput==null){
            Alert.alert(
                "Warning",
                "Field can't be empty",
                [
                  { text: "OK" }
                ]
            );
        }else if(emailInput==email1 && passwordInput==password1){
            this.props.navigation.navigate('Home')
            this.loginUser1()
            this.clearData()
        }else if(emailInput==email2 && passwordInput==password2){
            this.props.navigation.navigate('Home')
            this.loginUser2()
            this.clearData()
        }else{
            Alert.alert(
                "Warning",
                "Wrong email or password",
                [
                  { text: "OK" }
                ]
            );
        }    
    }

    async clearData(){
        this.setState({
            email : null,
            password : null
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <Image
                    source={require('../img/densTVLogo.png')}
                    resizeMode = 'contain'
                    style={styles.image}
                    />
                </View>
                <View style={styles.footer}>
                    <View style={styles.titleText}>
                         <Text style={styles.text2}>
                             Welcome!
                         </Text>
                    </View>
                    
                    <View style={styles.footerContent}>
                        <Icon 
                        style={{marginTop : 15}}
                        name="user" size={35} color="#000" />
                        <TextInput
                        placeholder="Email"
                        style={styles.inputText}
                        value={this.state.email}
                        onChangeText={
                            (text) => {
                                this.setState({ email: text })
                            }
                        }
                    />
                    </View>
                    <View style={styles.footerContent2}>
                        <Icon 
                        style={{marginTop : 17}}
                        name="key" size={25} color="#000" />
                        <TextInput
                        placeholder="Password"
                        secureTextEntry={this.state.hidePassword}
                        style={styles.inputText}
                        value={this.state.password}
                        onChangeText={
                            (text) => {
                                this.setState({ password: text })
                            }
                        }
                    />
                    </View>
                    <View style={styles.buttonView}>
                        <Button
                         title="Log in"
                         color="#000000"
                         onPress={()=>{this.loginFunction(this.state.email,this.state.password)}}
                        />
                    </View>
                </View>
            </SafeAreaView>

            
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#fff000'
    },
    header : {
        flex : 1,
       
    },
    footer : {
        flex: 3 ,
        backgroundColor : '#fff',
        borderTopLeftRadius : 30,
        borderTopRightRadius : 30 ,
    },
    image : {
        marginHorizontal : 20,
        height : '90%',
        width : '90%',
        justifyContent : 'center',
        alignItems : 'center',
        marginTop : 10,
        marginLeft : 15,
        marginRight : 5
        
    },
    text1 :{
        fontSize : 20
    },
    footerContent : {
        marginTop : 20,
        marginLeft : 20,
        flexDirection : 'row'
        
    },
    text2 :{
        fontSize : 30,
    },
    titleText :{
        alignItems : 'center',
        marginTop : 50
    },
    inputText: {
        height: 40,
        width : 300,
        margin: 12,
        borderWidth: 1,
    },
    footerContent2 : {
        marginLeft : 20,
        flexDirection : 'row'
        
    },  
    buttonView :{
        alignItems :'center',
        
        
    },

})
