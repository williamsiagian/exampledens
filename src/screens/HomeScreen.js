import React, { Component } from 'react'
import { SafeAreaView, Text, View , StyleSheet, TouchableOpacity, ScrollView, Image, Button, Alert} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';


export class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
           
        }
    }

    async thumbnailFirst(){
        var user1 = await AsyncStorage.getItem("user1")
        const isLogin = "login"

        if(user1==isLogin){
            this.props.navigation.navigate('First')
        }else{
            Alert.alert(
                "Restricted",
                "Dont have access to watch this video",
                [
                  { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        }

    }

    async thumbnailSecond(){
        var user2 = await AsyncStorage.getItem("user2")
        const isLogin = "login"

        if(user2==isLogin){
            this.props.navigation.navigate('Second')
        }else{
            Alert.alert(
                "Restricted",
                "Dont have access to watch this video",
                [
                  { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        }

    }

    async clearAccess(){
        const isLogout = "logout"
        await AsyncStorage.removeItem("user1")
        await AsyncStorage.removeItem("user2")
        this.props.navigation.navigate('Login')
    }

    render() {
        return (
            <SafeAreaView
            style={styles.container}>
                
                <ScrollView>
                    <View
                    style={styles.items}>
                        <TouchableOpacity
                         style={styles.opacityLayout}
                         onPress={()=>{this.thumbnailFirst()}}
                        >
                            <Text
                            style={styles.thumbnailText}> Justice League</Text>
                            <Image
                            source={require('../img/firstThumbnail.png')}
                            resizeMode = 'contain'
                            style={styles.thumbnailPicture}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                         style={styles.opacityLayout}
                         onPress={()=>{this.thumbnailSecond()}}
                        >
                            <Text
                            style={styles.thumbnailText}> The Avengers</Text>
                            <Image
                            source={require('../img/secondThumbnail.png')}
                            resizeMode = 'contain'
                            style={styles.thumbnailPicture}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                         style={styles.opacityLayout}
                         onPress={()=>{this.props.navigation.navigate('Third')}}
                        >
                            <Text
                            style={styles.thumbnailText}> Jurrasic park</Text>
                            <Image
                            source={require('../img/thirdThumbnail.png')}
                            resizeMode = 'contain'
                            style={styles.thumbnailPicture}
                            />
                        </TouchableOpacity>

                        <View
                        style={styles.buttonStyle}>
                            <Button
                            title="Log Out"
                            color="#000000"
                            onPress={()=>{this.clearAccess()}}
                            />
                        </View>

                        
                    </View>
                    
                </ScrollView>
                
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container :{
        flex : 1
    },
    thumbnailPicture :{
        height : 300,
        width : 300,
    },
    items :{
        alignItems : 'center'
    },
    buttonStyle :{
        marginTop : 20,
        marginBottom : 40
    },
    opacityLayout :{
        alignItems : 'center'
    },
    thumbnailText :{
        fontSize : 20,
        marginTop :20
    }
})