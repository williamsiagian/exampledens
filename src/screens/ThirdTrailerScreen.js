import React, { Component } from 'react'
import { Text, View, StyleSheet, SafeAreaView } from 'react-native'
import Video from 'react-native-video'

export class ThirdTrailerScreen extends React.Component {
    render() {
        return (
            <SafeAreaView>
                <View>
                <Video source={require('../video/jurrasicPark.mp4')}
                    controls
                    style={styles.backgroundVideo} />
                 </View>
            </SafeAreaView>
            
        )
    }
}
var styles = StyleSheet.create({
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height:300,
        width :300,
        marginTop : 20,
        marginLeft : 20
    },
})
