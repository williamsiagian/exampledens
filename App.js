import * as React from 'react';
import { View, Text, Button, ImageBackground, Image, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {LoginScreen} from './src/screens/LoginScreen';
import {HomeScreen} from './src/screens/HomeScreen';
import {FirstTrailerScreen} from './src/screens/FirstTrailerScreen';
import {SecondTrailerScreen} from './src/screens/SecondTrailerScreen';
import {ThirdTrailerScreen} from './src/screens/ThirdTrailerScreen';
import{Video} from 'react-native-video'


function SplashScreen({ navigation }) {
  setTimeout(()=>{
    navigation.replace('Login')
  },3000);
  return (

  
      <View
      style={{flex :1, alignItems:'center', justifyContent :'center'}}>
        
    <Image
        style={{width :  100, height : 100, alignSelf:'center'}}
        resizeMode = 'contain'
        source={require('./src/img/densLogoTosca.png')} />  


      </View>
 
    // <ImageBackground
    // style={{flex:1, padding : 10}}
    // resizeMode = 'contain'
    // source={require('./src/img/densLogoTosca.png')} >
    // </ImageBackground>
  );
}



const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={SplashScreen} options={{headerShown : false}}/>
        <Stack.Screen name="Login" component={LoginScreen} options={{headerShown : false}} />
        <Stack.Screen name="Home" component={HomeScreen} options={{headerShown : false}} />
        <Stack.Screen name="First" component={FirstTrailerScreen} options={{headerShown : false}} />
        <Stack.Screen name="Second" component={SecondTrailerScreen} options={{headerShown : false}} />
        <Stack.Screen name="Third" component={ThirdTrailerScreen} options={{headerShown : false}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
} export default App




